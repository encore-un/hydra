﻿using Constellation.Package;
using RestSharp;
using RestSharp.Deserializers;

namespace HydraCalls
{
    /// <summary>
    /// Static class used to push StateObjects to/from Constellation.
    /// Using OpenWeatherMap API, with specified token in package config.
    /// StateObjects lifetime is 5 mins for current weather and 3 hours for future weather (over 5 days).
    /// </summary>
    public static class Weather
    {
        private const string Uri = "http://api.openweathermap.org/data/2.5/";

        /// <summary>
        /// Add city's current weather to Constellation's StateObjects.
        /// </summary>
        /// <param name="city">City</param>
        /// <param name="country">Country</param>
        public static void AddCityToCurrentWeatherNotifier(string city, string country = "")
        {
            var request = country == "" ? CreateCurrentWeatherRequestAt(city) : CreateCurrentWeatherRequestAt(city, country);
            var client = new RestClient(Uri);
            client.ExecuteAsync(request, response =>
            {
                var name = city + "CurrentWeather";
                var deserializer = new JsonDeserializer();
                dynamic content = deserializer.Deserialize<dynamic>(response);
                PackageHost.PushStateObject(name, content, lifetime: 300);
                PackageHost.WriteInfo($"Added {city} to current weather notifier.");
            });
        }
        
        /// <summary>
        /// Add city's future weather (5 days) to Constellation's StateObjects.
        /// </summary>
        /// <param name="city">City</param>
        /// <param name="country">Country</param>
        public static void AddCityToFutureWeatherNotifier(string city, string country = "")
        {
            var request = country == "" ? CreateFutureWeatherRequestAt(city) : CreateFutureWeatherRequestAt(city, country);
            var client = new RestClient(Uri);
            client.ExecuteAsync(request, response =>
            {
                var name = city + "FutureWeather";
                var deserializer = new JsonDeserializer();
                dynamic content = deserializer.Deserialize<dynamic>(response);
                PackageHost.PushStateObject(name, content, lifetime: 3600*3);
                PackageHost.WriteInfo($"Added {city} to future weather notifier.");
            });
        }

        private static RestRequest CreateFutureWeatherRequestAt(string city, string country = "")
        {
            var request = new RestRequest("forecast", Method.GET);
            var query = 
                country == "" ?
                city + "," + country :
                city;

            request.AddParameter("q", query);
            request.AddParameter("appid", PackageHost.GetSettingValue<string>("weather_token"));
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };

            return request;
        }

        private static RestRequest CreateCurrentWeatherRequestAt(string city, string country = "")
        {
            var request = new RestRequest("weather", Method.GET);
            var query = 
                country == "" ? 
                city + "," + country :
                city;

            request.AddParameter("q", query);
            request.AddParameter("appid", PackageHost.GetSettingValue<string>("weather_token"));
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };

            return request;
        }

        private static RestRequest CreateCurrentWeatherRequestAt(float lat, float lon)
        {
            var request = new RestRequest("weather", Method.GET);
            request.AddParameter("lat", lat);
            request.AddParameter("lon", lon);
            request.AddParameter("appid", PackageHost.GetSettingValue<string>("weather_token"));
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };

            return request;
        }
    }
}
