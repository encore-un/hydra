﻿using System;
using Constellation.Package;
using RestSharp;
using RestSharp.Deserializers;

namespace HydraCalls
{
    /// <summary>
    /// Static class searching for news by date and by keyword using the New York Times API.
    /// </summary>
    public static class News
    {
        private const string Uri = "https://api.nytimes.com/svc/search/v2/articlesearch.json";

        /// <summary>
        /// Search news by keywords, between today and 'from' parameter.
        /// </summary>
        /// <param name="keyword">Keywords string. Multiple keywords are seperated by spaces, can perform OR operation
        /// using '+' between keywords.</param>
        /// <param name="from">Searching news between today and 'from' date. (format: YYYYMMDD)</param>
        public static void SearchNews(string keyword, string from)
        {
            var today = DateTime.Today;
            var month = today.Month < 10 ? "0" + today.Month : today.Month.ToString();
            var todayStr = string.Concat(today.Year.ToString(), month, today.Day.ToString());

            var client = new RestClient(Uri);
            var request = new RestRequest(Method.GET);
            request.AddParameter("api-key", PackageHost.GetSettingValue<string>("nyt_token"));
            request.AddParameter("q", keyword);
            request.AddParameter("begin_date", from);
            request.AddParameter("end_date", todayStr);
            request.AddParameter("sort", "newest");
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };

            client.ExecuteAsync(request, response =>
            {
                var deserializer = new JsonDeserializer();
                dynamic content = deserializer.Deserialize<dynamic>(response);
                var name = keyword + "RelatedNews";
                PackageHost.PushStateObject(name, content);
                PackageHost.WriteInfo($"Added {keyword} related news.");
            });
        }
    }
}
