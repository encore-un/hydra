﻿using System;
using Constellation.Package;
using RestSharp;
using RestSharp.Deserializers;
using YSQ.core.Quotes;

namespace HydraCalls
{
    /// <summary>
    /// Static class which gets the stock quotes from the Yahoo Finance and Fixer APIs.
    /// </summary>
    public static class Finance
    {
        private static readonly QuoteService QuoteService = new QuoteService();
        private const string ExchangeRatesUri = "http://api.fixer.io/";

        /// <summary>
        /// Get stock quotes from a company.
        /// </summary>
        /// <param name="symbol">The company's symbol.</param>
        public static void GetFinanceDataFrom(string symbol)
        {
            var quote = QuoteService.Quote(symbol).Return(
                QuoteReturnParameter.Symbol,
                QuoteReturnParameter.Name,
                QuoteReturnParameter.LatestTradePrice,
                QuoteReturnParameter.LatestTradeTime,
                QuoteReturnParameter.LatestTradeDate);

            
            Console.WriteLine($"{quote.Symbol} - {quote.Name} - {quote.LatestTradePrice} - {quote.LatestTradeTime} - {quote.LatestTradeDate}");
            var name = quote.Symbol + " quote";
            PackageHost.PushStateObject(name, quote, lifetime: 3600*2);
        }

        /// <summary>
        /// Get stock quotes from multiple companies.
        /// </summary>
        /// <param name="symbols">The companies' symbols.</param>
        public static void GetFinanceDataFrom(string[] symbols)
        {
            var quotes = QuoteService.Quote(symbols).Return(
                QuoteReturnParameter.Symbol,
                QuoteReturnParameter.Name,
                QuoteReturnParameter.LatestTradePrice,
                QuoteReturnParameter.LatestTradeTime,
                QuoteReturnParameter.LatestTradeDate);

            foreach (var quote in quotes)
            {
                Console.WriteLine($"{quote.Symbol} - {quote.Name} - {quote.LatestTradePrice} - {quote.LatestTradeTime} - {quote.LatestTradeDate}");
                string name = quote.Symbol + " quote";
                PackageHost.PushStateObject(name, quote, lifetime: 3600 * 2);
            }
        }

        /// <summary>
        /// Get current exchange rate for a currency against another.
        /// </summary>
        /// <param name="symbol">Currency's symbol (EUR, USD...)</param>
        /// <param name="against">Base currency's symbol (EUR, USD...)</param>
        public static void GetCurrentExchangeRate(string symbol, string against)
        {
            var client = new RestClient(ExchangeRatesUri);
            var request = new RestRequest("latest", Method.GET);
            request.AddParameter("symbols", symbol);
            request.AddParameter("base", against);
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };

            client.ExecuteAsync(request, response =>
            {
                var deserializer = new JsonDeserializer();
                dynamic content = deserializer.Deserialize<dynamic>(response);
                var name = symbol + "ExchangeRateAgainst" + against;
                PackageHost.PushStateObject(name, content, lifetime: 3600 * 24);
                PackageHost.WriteInfo($"Added exchange rate of {symbol} against {against}.");
            });
        }

        /// <summary>
        /// Get exchange rates of a given day against a given currency.
        /// </summary>
        /// <param name="date">The date, between 1999 and today (format: 1999-12-31)</param>
        /// <param name="against">The base currency's symbol (EUR, USD...)</param>
        public static void GetHistoricalExchangeRates(string date, string against)
        {
            var client = new RestClient(ExchangeRatesUri);
            var request = new RestRequest(date, Method.GET);
            request.AddParameter("base", against);
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };

            client.ExecuteAsync(request, response =>
            {
                var deserializer = new JsonDeserializer();
                dynamic content = deserializer.Deserialize<dynamic>(response);
                var name = "ExchangeRatesAgainst" + against + "For" + date;
                PackageHost.PushStateObject(name, content);
                PackageHost.WriteInfo($"Added exchange rates for {date} against {against}.");
            });
        }
    }
}
