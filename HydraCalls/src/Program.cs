﻿using Constellation.Package;
using System;

namespace HydraCalls
{
    public class Program : PackageBase
    {
        static void Main(string[] args)
        {
            PackageHost.Start<Program>(args);
        }

        public override void OnStart()
        {
            PackageHost.WriteInfo($"Package starting - IsRunning: {PackageHost.IsRunning} - IsConnected: {PackageHost.IsConnected}");
            PackageHost.LastStateObjectsReceived += (s, e) =>
            {
                foreach (var so in e.StateObjects)
                {
                    PackageHost.PushStateObject(so.Name, so.Value, lifetime: so.Lifetime);
                }
            };
        }

        /// <summary>
        /// Get stock quotes from one or multiple companies.
        /// </summary>
        /// <param name="symbol">The company's (or companies') symbol.
        /// If multiple companies, format is GOOG,AAPL,... (symbols separated by commas)</param>
        [MessageCallback]
        private void GetFinanceDataFrom(string symbol)
        {
            if (symbol.Contains(","))
                Finance.GetFinanceDataFrom(symbol.Split(Convert.ToChar(",")));
            else
                Finance.GetFinanceDataFrom(symbol);
        }

        /// <summary>
        /// Get current exchange rate for a currency against another.
        /// </summary>
        /// <param name="symbol">Currency's symbol (EUR, USD...)</param>
        /// <param name="against">Base currency's symbol (EUR, USD...)</param>
        [MessageCallback]
        private void GetCurrentExchangeRate(string symbol, string against)
        {
            Finance.GetCurrentExchangeRate(symbol, against);
        }

        /// <summary>
        /// Get exchange rates of a given day against a given currency.
        /// </summary>
        /// <param name="date">The date, between 1999 and today (format: 1999-12-31)</param>
        /// <param name="against">The base currency's symbol (EUR, USD...)</param>
        [MessageCallback]
        private void GetHistoricalExchangeRates(string date, string against)
        {
            Finance.GetHistoricalExchangeRates(date, against);
        }

        /// <summary>
        /// Add a city to the current weather notifier.
        /// </summary>
        /// <param name="city">The city</param>
        /// <param name="country">The country (format: fr, uk, us...)</param>
        [MessageCallback]
        private void AddCityToCurrentWeatherNotifier(string city, string country)
        {
            Weather.AddCityToCurrentWeatherNotifier(city, country);
        }

        /// <summary>
        /// Add a city to the future weather (over 5 days) notifier.
        /// </summary>
        /// <param name="city">The city</param>
        /// <param name="country">The country (format: fr, uk, us...)</param>
        [MessageCallback]
        private void AddCityToFutureWeatherNotifier(string city, string country)
        {
            Weather.AddCityToFutureWeatherNotifier(city, country);
        }
    }
}
