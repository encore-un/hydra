﻿angular.module('rule')
    .component('newRule',
    {
        templateUrl: "rule/rule.template.html",
        controller: [
            "$scope", "$location", "$route", "$routeParams", "connectionConstellation", "$state", function($scope,
                $location,
                $route,
                $routeParams,
                connectionConstellation) {
           $scope.factoryConstellation = connectionConstellation;

           $scope.ruleGroupName = $routeParams.ruleGroup;

           $scope.factoryConstellation.setRuleGroupInSelection($scope.ruleGroupName);

           $scope.modifiedRule = false;

           $scope.saveSuccess = false;

           $scope.firstTry = false;

           $scope.rule = {
               Condition: {}
           };
           $scope.rule.Name = "New rule name"
           $scope.updateCondition = function (condition) {
               $scope.rule.Condition = condition;
               $scope.modifiedRule = true;
           }

            $scope.saveRule = function() {
                $scope.firstTry = true;
                $scope.factoryConstellation.callWhenReady(function() {
                    $scope.factoryConstellation.getConstellation()
                        .sendMessageWithSaga({ Scope: "Package", Args: ["Hydra"] },
                            "AddRule",
                            [$routeParams.ruleGroup, angular.toJson($scope.rule)],
                            function(response) {
                                $scope.$apply(function() {
                                    $scope.saveSuccess = response.Data;
                                });
                            });
                });
            };

            $scope.redirectHome = function() {
                $scope.factoryConstellation.updateRuleGroups();
                $scope.factoryConstellation.setRuleGroupInSelection($scope.factoryConstellation.getSlogan());
                $location.path("#/");
            };
        }
    ]
});
