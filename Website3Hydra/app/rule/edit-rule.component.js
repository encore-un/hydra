﻿angular.module('rule')
    .component('editRule', {
        templateUrl: "rule/rule.template.html",
        controller: [
           "$scope","$location", "$route", "$routeParams","connectionConstellation", function ($scope,$location,$route,$routeParams,connectionConstellation ) {
               $scope.factoryConstellation = connectionConstellation;
               $scope.ruleGroupName = $routeParams.ruleGroup;

               $scope.factoryConstellation.setRuleGroupInSelection($scope.ruleGroupName);

               var ruleName = $routeParams.ruleName;

               $scope.saveSuccess = false;

               $scope.firstTry = false;

               $scope.rule = {
                   Condition: {}
               };

               $scope.factoryConstellation.callWhenReady(function () {
                   $scope.factoryConstellation.getRules(function (allRules) {
                       $scope.$apply(function() {
                           var rules = allRules;

                           $.each(rules,
                               function(key, val) {
                                   if (val.Name === $scope.ruleGroupName && val.Rules[ruleName] != "undefined") {

                                       for (var member in $scope.rule) delete $scope.rule[member];

                                       for (var k in val.Rules[ruleName]) $scope.rule[k] = val.Rules[ruleName][k];
                                   }
                               });
                       });
                   });
               });

                $scope.updateCondition = function(condition) {
                    $scope.rule.Condition = condition;
                }

                $scope.saveRule = function () {
                    $scope.firstTry = true;
                    $scope.factoryConstellation.callWhenReady(function () {
                    $scope.factoryConstellation.getConstellation().sendMessageWithSaga({ Scope: "Package", Args: ["Hydra"] },
                        "EditRule",
                        [$routeParams.ruleGroup, angular.toJson($scope.rule)],
                        function(response) {
                            $scope.$apply(function() {
                                $scope.saveSuccess = response.Data;
                            });
                        });
                });
                };

               $scope.redirectHome = function() {
                   $scope.factoryConstellation.updateRuleGroups();
                   $scope.factoryConstellation.setRuleGroupInSelection($scope.factoryConstellation.getSlogan());
                   $location.path("#/");
               }
            }
        ]
    });
