var app = angular.module("hydraApp", ["ngRoute", "ngAnimate","ngConstellation", "ui.bootstrap","ui.router", "rule", "condition", "valueProvider", "pills"]);

app.config(["$routeProvider","$locationProvider", function ($routeProvider, $locationProvider) {

   $routeProvider
    .when('/', {
      templateUrl: 'home.html',
      controller: 'homeController'
    })

    .when('/edit/:ruleGroup/:ruleName', {
        template: '<edit-rule></edit-rule>'
    })

    .when('/new/:ruleGroup', {
        template: '<new-rule></new-rule>'
    })

    .when('/saved', {
      templateUrl: 'saved.html',
      controller: 'savedController'
    })

    .otherwise({
      redirectTo: '/'
    });
}]);

app.service("sharedProperties", function () {
    var _valueProvider = {
        name: "None",
        key: "none"
    };

    var _methods = [{
        name: 'Constant',
        key: 'constant'
    }, {
        name: 'State object value',
        key: 'stateObjectValue'
    }];

    return {
        getValueProvider: function () {
            return _valueProvider;
        },

        setValueProvider: function (value) {
            _valueProvider = value;
        },

        getMethods: function () {
            return _methods;
        }
    };
});

app.factory('connectionConstellation', [
  "constellationConsumer", "constellationController", function(constellation, controller) {

        var _constellation = constellation,
            _controller = controller,
            _sentinelValue,
            _ruleGroups = [],
            _packageList = [],
            _stateObjectList = [],
            _stateObjectNameList = [],
            _stateObjectSaved,
            _allConditions = [],
            _conditionsName = [],
            _callOnReady = [],
            _connectionReady = false,
            _callWhenRuleGroupUpdated = [],
            _ruleGroupInSelection,
            _slogan = "Hydra - Make it simple";

        _constellation.intializeClient("http://localhost:8088", "84907f1b1419c1bbf9616e08467d15d262ca9cc1", "Hydra");
        _controller.intializeClient("http://localhost:8088", "84907f1b1419c1bbf9616e08467d15d262ca9cc1", "Hydra");

        return {
            getConstellation: function() { return _constellation; },

            getController: function() { return _controller; },

            getSentinelValue: function() { return _sentinelValue; },

            getRuleGroups: function() { return _ruleGroups; },

            getPackageList: function() { return _packageList; },

            getStateObjectList: function() { return _stateObjectList; },

            getStateObjectSaved: function() { return _stateObjectSaved; },

            getStateObjectNameList: function() { return _stateObjectNameList; },

            getAllConditions: function() { return _allConditions; },

            getConditionsName: function() { return _conditionsName; },

            getConnectionReady: function() { return _connectionReady; },

            setConnectionReady: function(connectionState) { _connectionReady = connectionState; },

            getCallOnReady: function() { return _callOnReady; },

            setCallOnReady: function(value) { _callOnReady = value; },

            getRuleGroupInSelection: function () { return _ruleGroupInSelection; },

            setRuleGroupInSelection: function (value) { _ruleGroupInSelection = value },

            getSlogan : function() { return _slogan; },

        callWhenReady: function(callback) {
            if (_connectionReady) {
                callback();
            } else {
                _callOnReady.push(callback);
            }
        },

        callAll: function() {
            $.each(_callOnReady, function (key, callback) {
                callback();
            });

            _callOnReady = [];
        },

        getRules: function (callback) {
            constellation.sendMessageWithSaga({ Scope: "Package", Args: ["Hydra"] },
              "GetRules",
              [],
              function (response) {
                  _ruleGroups = response.Data;
                  callback(_ruleGroups);
              }
            );

        },

        updateRuleGroups() {
            constellation.sendMessageWithSaga({ Scope: "Package", Args: ["Hydra"] },
              "GetRules",
              [],
              function (response) {
                  _ruleGroups = response.Data;
                      $.each(_callWhenRuleGroupUpdated, function (key, callback) {
                      callback(_ruleGroups);
                  });
              }
            );
        },

        callWhenRuleGroupUpdated: function(callback) {
            _callWhenRuleGroupUpdated.push(callback);
        },

        updatePackageList: function(sentinel) {
          var packageList = [];
          _sentinelValue = sentinel;

          _constellation.sendMessageWithSaga({ Scope: "Package", Args: ["Hydra"] },
            "GetPackagesList",
            sentinel,
            function(response) {
              packageList = response.Data;
              _packageList = packageList;
            }
          );

          _packageList = packageList;
        },

        getStateObjects: function(package) {
          var stateObjectNameList = [];
          var stateObjectList = [];

          constellation.sendMessageWithSaga({ Scope: "Package", Args: ["Hydra"] },
            "GetStateObjectList",
            [_sentinelValue, package],
            function(response) {
              stateObjectList = response.Data;

              for (var i = 0; i < response.Data.length; i++) {
                stateObjectNameList.push(response.Data[i].Name);
              }

              _stateObjectList = stateObjectList;
              _stateObjectNameList = stateObjectNameList;
            }
          );

          _stateObjectList = stateObjectList;
          _stateObjectNameList = stateObjectNameList;
        },

        selectStateObject: function(selected) {
          $.each(_stateObjectList, function(id, stateObject) {
            if (stateObject.Name === selected) {
              _stateObjectSaved = stateObject;
            }
          });
        },

        createConditions: function (callback) {
            constellation.sendMessageWithSaga({ Scope: "Package", Args: ["Hydra"] },
                "GetConditionsSchemas",
                [],
                function (response) {
                    var allConditions = [];
                    var conditionsName = [];
                    $.each(response.Data,
                    function (conditionName, conditionDescriber) {
                        allConditions[conditionName] = conditionDescriber;
                        conditionsName.push(conditionName);
                    });
                    _allConditions = allConditions;
                    _conditionsName = conditionsName;
                    callback(_conditionsName,_allConditions);
                });
        },

      };
    }
]);

app.controller("hydraController", ["$scope", "$location", "$route", "$routeParams", "connectionConstellation" , "sharedProperties", function ($scope, $location, $route, $routeParams, connectionConstellation, sharedProperties) {
  var constellation = connectionConstellation.getConstellation();
  var controller = connectionConstellation.getController();
  $scope.isCollapsed = true;

  $scope.factoryConstellation = connectionConstellation;

  $scope.factoryConstellation.setRuleGroupInSelection($scope.factoryConstellation.getSlogan());

  connectionConstellation.getConstellation().onConnectionStateChanged(function (change) {
      if (change.newState === $.signalR.connectionState.connected) {
          $scope.factoryConstellation.setConnectionReady(true);
          $scope.factoryConstellation.callAll();
      } else {
          $scope.factoryConstellation.setConnectionReady(false);
      }
  });

    $scope.initJumbotron = function() {
        $scope.factoryConstellation.setRuleGroupInSelection($scope.factoryConstellation.getSlogan());
    }

    constellation.connect();
    controller.connect();
}]);

app.controller('homeController', function($scope, $route, $location) {
  $scope.location = $location.url();
});

app.controller('newController', function($scope, $location) {
  $scope.location = $location.url();
});

app.controller('savedController', function($scope, $route, $location) {
  $scope.location = $location.url();
});
