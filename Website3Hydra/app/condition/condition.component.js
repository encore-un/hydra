﻿angular.module('condition')
    .component('condition',
    {
        templateUrl: "condition/condition.template.html",
        controller:
            function ($scope, connectionConstellation) {
                var ctrl = this;

                $scope.setCondition = function () {
                    $scope.condition = ctrl.defaultValue;
                    if (typeof $scope.condition.ConditionName === "undefined") {
                        $scope.label = "Choose a condition type";
                    } else {
                        $scope.label = $scope.condition.ConditionName;
                    }
                }

                $scope.setCondition();

                $scope.condition.DefaultValue = false;

                $scope.isCollapsed = function() {
                    return $scope.argumentName !== $scope.ConditionName && $scope.argumentName !== $scope.DefaultValue
                }

                // my little trick
                $scope.$watch(function() {
                        return ctrl.defaultValue;
                    },
                    function() {
                        $scope.setCondition();
                    });

                $scope.factoryConstellation = connectionConstellation;

                $scope.factoryConstellation.callWhenReady(function () {

                    $scope.factoryConstellation.createConditions(function (conditionsName, conditions) {
                        $scope.$apply(function () {
                            $scope.conditionsName = conditionsName;
                            $scope.conditionsDescription = conditions;
                        });
                    });
                });


                $scope.updateArgument = function (name, value) {
                    $scope.condition[name] = value;
                    $scope.onConditionUpdate();
                }

                $scope.onConditionUpdate = function () {
                    ctrl.onUpdate({data : $scope.condition});
                }

                $scope.clearParameters = function(doNotClearTheseFields) {

                    doNotClearTheseFields.push("ConditionName");
                    doNotClearTheseFields.push("DefaultValue");

                    $.each($scope.condition, function (fieldName, field) {
                        if (doNotClearTheseFields.indexOf(fieldName) === -1)
                            delete $scope.condition[fieldName];
                    });
                }

                $scope.onConditionNameSelected = function(conditionNameSelected) {
                    $scope.label = conditionNameSelected;
                    $scope.condition.ConditionName = conditionNameSelected;
                    $scope.getConditionsOperand(conditionNameSelected);
                }

                $scope.getConditionsOperand = function(conditionNameSelected) {
                    var conditionSelected = $scope.factoryConstellation.getAllConditions()[conditionNameSelected];
                    var requiredFields = conditionSelected.required;

                    $scope.clearParameters(requiredFields);

                    $.each(requiredFields, function(id, fieldName) {
                        if (fieldName === "ConditionName" || fieldName === "DefaultValue") return; //ignore condition name

                        if (conditionSelected.properties[fieldName].type === "object") {

                            if (typeof $scope.condition[fieldName] === "undefined")
                                $scope.condition[fieldName] = {};

                            if (typeof conditionSelected.properties[fieldName].properties["Provider"] != "undefined") {
                                if (typeof $scope.condition[fieldName].Provider === "undefined")
                                    $scope.condition[fieldName].Provider = null;
                            }

                            else if (typeof conditionSelected.properties[fieldName].properties["ConditionName"] != "undefined") {
                                if (typeof $scope.condition[fieldName].Provider === "undefined")
                                    $scope.condition[fieldName].ConditionName = null;
                            }
                        }
                    });
                }

            },


        bindings: {
            defaultValue: "<",
            onUpdate: "&"
        }
});
