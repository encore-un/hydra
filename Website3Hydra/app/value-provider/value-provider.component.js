angular.module('valueProvider')

.component('valueProvider', {
  templateUrl: 'value-provider/value-provider.template.html',
  controller: function ($scope) {

    var ctrl = this;
    
    $scope.valueProviders = {
        "Constant": {},
        "StateObjectVariable": {}
    }

    $scope.status = {
        isopen: false,
        label: "Get value from",
        valueProviderName: null
    };

    if (typeof ctrl.defaultValue !== "undefined") {
        var valueProviderName = ctrl.defaultValue.Provider;

        $scope.status.valueProviderName = valueProviderName;

        if(valueProviderName != null)
            $scope.valueProviders[valueProviderName] = ctrl.defaultValue;
    }

    $scope.toggleDropdown = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.status.isopen = !$scope.status.isopen;
    };

    $scope.selectValueProvider = function (valueProviderName) {
        $scope.status.valueProviderName = valueProviderName;
        $scope.status.label = valueProviderName;
    };

    $scope.onValueProviderUpdate = function (value) {
        var providerName = $scope.status.valueProviderName;
        value.Provider = providerName;

        $scope.valueProviders[providerName] = value;
        $scope.status.valueProviderName = providerName;
        $scope.status.label = providerName;

        ctrl.onUpdate({ data: value });
    };
  },
  bindings:{
    defaultValue: "<",
    onUpdate: "&"
  }
});
