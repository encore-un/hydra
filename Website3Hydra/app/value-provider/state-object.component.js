Array.prototype.clean = function(deleteValue) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == deleteValue) {
      this.splice(i, 1);
      i--;
    }
  }
  return this;
};

angular.module('valueProvider')

.component('stateObject', {
  templateUrl: 'value-provider/state-object.template.html',

  controller: function($scope, $timeout, $element, connectionConstellation) {
    var ctrl = this;

    $scope.stateObject = {};

    $timeout(function() { $scope.$apply(function() { $scope.stateObject.JPath = "$"; }); });
    $scope.factoryConstellation = connectionConstellation;
    $scope.sentinelNameList = [];
    $scope.attrs = "";
    $scope.sentinelClicked = false;
    $scope.packageClicked = false;
    $scope.stateObjectClicked = false;
    $scope.jpathClicked = false;
    $scope.stateObjectKeys = [];

    $scope.factoryConstellation.callWhenReady(function () {
      $scope.factoryConstellation.getController().onUpdateSentinelsList(function(sentinels) {
        $.each(sentinels.List, function(id, sentinel) {
          if ($scope.sentinelNameList.indexOf(sentinel.Description.SentinelName) === -1) {
            $scope.sentinelNameList.push(sentinel.Description.SentinelName);
          }
        });
      });
      $scope.factoryConstellation.getController().requestSentinelsList();
    });

    $scope.toggleOptions = function(type) {
      if (type === "sentinel") {
        $scope.sentinelClicked = !$scope.sentinelClicked;
      } else if (type === "package") {
        $scope.packageClicked = !$scope.packageClicked;
      } else if (type === "stateObject") {
        $scope.stateObjectClicked = !$scope.stateObjectClicked;
      } else if (type === "jpath") {
        $scope.jpathClicked = !$scope.jpathClicked;
      }
    };

    var defaultStateObject = ctrl.defaultValue;
    if (typeof defaultStateObject != "undefined") {
        $scope.stateObject.SentinelName = defaultStateObject.SentinelName;
        $scope.stateObject.PackageName = defaultStateObject.PackageName;
        $scope.stateObject.StateObjectName = defaultStateObject.StateObjectName;
        $scope.stateObject.JPath = defaultStateObject.JPath;
    }

    $scope.onSentinelNameChange = function () {
        $scope.factoryConstellation.updatePackageList($scope.stateObject.SentinelName);
        ctrl.onUpdate({ data: $scope.stateObject });
    };

    $scope.onPackageNameChange = function () {
        $scope.factoryConstellation.getStateObjects($scope.stateObject.PackageName);
        ctrl.onUpdate({ data: $scope.stateObject });
    };

    $scope.onStateObjectNameChange = function () {
        $scope.factoryConstellation.selectStateObject($scope.stateObject.StateObjectName);
        var stateObjectSaved = $scope.factoryConstellation.getStateObjectSaved();
        var keys;

        if (typeof stateObjectSaved != "undefined") {
          console.log(stateObjectSaved.Value);
          keys = Object.keys(stateObjectSaved.Value);
          $.each(keys, function(id, key) {
            var jpath = "$." + keys[id];
            keys.push($scope.isObjectOrArray(stateObjectSaved.Value, keys, id, jpath));
            keys[id] = jpath;
          });
          keys.clean(undefined);
          $scope.stateObjectKeys = keys;
          console.log($scope.stateObjectKeys);
        }
        ctrl.onUpdate({ data: $scope.stateObject });
    };

    $scope.isObjectOrArray = function(obj, keys, id, previousJPath) {
      if (typeof obj[keys[id]] !== "object") return;
      var jpath;

      if (Array.isArray(obj[keys[id]])) {
        $.each(obj[keys[id]], function(subid, subobj) {
          jpath = previousJPath + "[" + subid + "]";
          keys.push(jpath);
        });
      } else {
        $.each(obj[keys[id]], function(subid, subkey) {
          jpath = previousJPath + "." + subid;
          keys.push(jpath);
        });
      }
    };

    $scope.onJPathChange = function () {
        console.log($scope.stateObjectKeys);
        ctrl.onUpdate({ data: $scope.stateObject });
    };

    $scope.startsWith = function(state, viewValue) {
        return state.substr(0, viewValue.length).toLowerCase() == viewValue.toLowerCase();
    };
  },

  bindings: {
  	defaultValue: "<",
  	onUpdate: "&"
  }
})

.directive("typeaheadStaticSentinel", function($timeout) {
  return {
    restrict: 'A',
    require: ['ngModel'],
    link: function(scope, element, attrs, ctrls) {
      var ngModel = ctrls[0];
      scope.attrs = attrs.name;
      var deregisterFn = scope.$watch(watchExpr, listener);

      ngModel.$parsers.push(parserFn);

      scope.$on('$destroy',function() {
        deregisterFn();
      });

      function watchExpr(scope) {
        return scope.sentinelClicked;
      }

      function listener(isClicked) {
        if (isClicked) {
          $timeout(showOptions);
        }
      }

      function parserFn(inputValue) {
        // don't put empty space to model and save the current modelIf present
        if (inputValue === ' ') {
          return ngModel.$modelValue ? ngModel.$modelValue : '';
        }
        return inputValue;
      }

      function showOptions() {
        var viewValue = ngModel.$viewValue;

        // restore to null value so that the typeahead can detect a change
        if (ngModel.$viewValue == ' ') {
          ngModel.$setViewValue('');
        }

        // force trigger the popup
        ngModel.$setViewValue(' ');

        // set the actual value in case there was already a value in the input
        // this is commented because we want show all options
        // ngModel.$setViewValue(viewValue || ' ');
        element[0].focus();
        scope.toggleOptions("sentinel");
      }
    }
  }
})

.directive("typeaheadStaticPackage", function($timeout) {
  return {
    restrict: 'A',
    require: ['ngModel'],
    link: function(scope, element, attrs, ctrls) {
      var ngModel = ctrls[0];
      scope.attrs = attrs.name;
      var deregisterFn = scope.$watch(watchExpr, listener);

      ngModel.$parsers.push(parserFn);

      scope.$on('$destroy',function() {
        deregisterFn();
      });

      function watchExpr(scope) {
        return scope.packageClicked;
      }

      function listener(isClicked) {
        if (isClicked) {
          $timeout(showOptions);
        }
      }

      function parserFn(inputValue) {
        // don't put empty space to model and save the current modelIf present
        if (inputValue === ' ') {
          return ngModel.$modelValue ? ngModel.$modelValue : '';
        }
        return inputValue;
      }

      function showOptions() {
        var viewValue = ngModel.$viewValue;

        // restore to null value so that the typeahead can detect a change
        if (ngModel.$viewValue == ' ') {
          ngModel.$setViewValue('');
        }

        // force trigger the popup
        ngModel.$setViewValue(' ');

        // set the actual value in case there was already a value in the input
        // this is commented because we want show all options
        // ngModel.$setViewValue(viewValue || ' ');
        element[0].focus();
        scope.toggleOptions("package");
      }
    }
  }
})

.directive("typeaheadStaticStateobject", function($timeout) {
  return {
    restrict: 'A',
    require: ['ngModel'],
    link: function(scope, element, attrs, ctrls) {
      var ngModel = ctrls[0];
      scope.attrs = attrs.name;
      var deregisterFn = scope.$watch(watchExpr, listener);

      ngModel.$parsers.push(parserFn);

      scope.$on('$destroy',function() {
        deregisterFn();
      });

      function watchExpr(scope) {
        return scope.stateObjectClicked;
      }

      function listener(isClicked) {
        if (isClicked) {
          $timeout(showOptions);
        }
      }

      function parserFn(inputValue) {
        // don't put empty space to model and save the current modelIf present
        if (inputValue === ' ') {
          return ngModel.$modelValue ? ngModel.$modelValue : '';
        }
        return inputValue;
      }

      function showOptions() {
        var viewValue = ngModel.$viewValue;

        // restore to null value so that the typeahead can detect a change
        if (ngModel.$viewValue == ' ') {
          ngModel.$setViewValue('');
        }

        // force trigger the popup
        ngModel.$setViewValue(' ');

        // set the actual value in case there was already a value in the input
        // this is commented because we want show all options
        // ngModel.$setViewValue(viewValue || ' ');
        element[0].focus();
        scope.toggleOptions("stateObject");
      }
    }
  }
})

.directive("typeaheadStaticJpath", function($timeout) {
  return {
    restrict: 'A',
    require: ['ngModel'],
    link: function(scope, element, attrs, ctrls) {
      var ngModel = ctrls[0];
      scope.attrs = attrs.name;
      var deregisterFn = scope.$watch(watchExpr, listener);

      ngModel.$parsers.push(parserFn);

      scope.$on('$destroy',function() {
        deregisterFn();
      });

      function watchExpr(scope) {
        return scope.jpathClicked;
      }

      function listener(isClicked) {
        if (isClicked) {
          $timeout(showOptions);
        }
      }

      function parserFn(inputValue) {
        // don't put empty space to model and save the current modelIf present
        if (inputValue === ' ') {
          return ngModel.$modelValue ? ngModel.$modelValue : '';
        }
        return inputValue;
      }

      function showOptions() {
        var viewValue = ngModel.$viewValue;

        // restore to null value so that the typeahead can detect a change
        if (ngModel.$viewValue == ' ') {
          ngModel.$setViewValue('');
        }

        // force trigger the popup
        ngModel.$setViewValue(' ');

        // set the actual value in case there was already a value in the input
        // this is commented because we want show all options
        // ngModel.$setViewValue(viewValue || ' ');
        element[0].focus();
        scope.toggleOptions("jpath");
      }
    }
  }
})

.filter("typeaheadStaticFilter", function(filterFilter) {
  return filterFn;

  function filterFn(input, params) {
    if (params===' ') {
      return input;
    }

    return filterFilter(input, params);
  }
});
