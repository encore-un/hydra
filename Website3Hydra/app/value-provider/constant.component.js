angular.module('valueProvider')
.component('constant', {
  templateUrl: 'value-provider/constant.template.html',
  controller: function constantController($scope, $element, $attrs) {

    var ctrl = this;
    
    if (typeof this.defaultValue != "undefined") {
        $scope.value = this.defaultValue.Value;
    }

    $scope.handleValueChange = function () {
        if (!isNaN($scope.value))
            $scope.value = parseFloat($scope.value);

      ctrl.onUpdate({
        data: {
            Value: $scope.value
        }
      });
    };
  },
  bindings:{
      defaultValue: "<",
      onUpdate: "&"
  }
});
