﻿angular.module('pills')
    .component('pillsRulesGroup',
    {
        replace: true,
        templateUrl: "pills/pills.template.html",
        controller: function($scope, $location, $uibModal, connectionConstellation) {

            $scope.factoryConstellation = connectionConstellation;
            $scope.isCollapsed = true;
            $scope.localRuleGroups = [];

            $scope.factoryConstellation.callWhenReady(function() {
                $scope.factoryConstellation.getRules(function(ruleGroups) {
                    $scope.$apply(function() {
                        $scope.ruleGroups = ruleGroups;
                        $scope.clearLocalRuleGroups();
                    });
                });
            });

            $scope.factoryConstellation.callWhenRuleGroupUpdated(function(ruleGroups) {
                $scope.$apply(function() {
                    $scope.ruleGroups = ruleGroups;
                    $scope.clearLocalRuleGroups();
                });
            });

            $scope.clearLocalRuleGroups = function() {
                $.each($scope.ruleGroups,
                    function(ruleGroupName) {
                        var index = $scope.localRuleGroups.indexOf(ruleGroupName);
                        if (index !== -1) {
                            $scope.localRuleGroups.splice(index, 1);
                        }
                    });
            };
            $scope.openCollapside = function(rulesGroupChecked) {
                if ($scope.rulesGroupSelected != undefined) {
                    if ($scope.rulesGroupSelected === rulesGroupChecked) {
                        return false;
                    } else {
                        return true;
                    }
                }
                return true;
            };

            $scope.selectRuleGroup = function (ruleGroupName) {
                if ($scope.rulesGroupSelected !== ruleGroupName)
                    $scope.rulesGroupSelected = ruleGroupName;
                else {
                    $scope.rulesGroupSelected = undefined;
                }
            };

            $scope.addRuleGroup = function(name) {
                $scope.localRuleGroups.push($scope.name);
            };

            $scope.open = function(ruleGroupName, ruleName) {
                var isRemovingRuleGroup = arguments.length === 1 ? true : false;
                var pillsScope = $scope;
                var modalInstance = $uibModal.open({
                    templateUrl: "pills/confirmation-modal.template.html",
                    controller: function($scope, $uibModalInstance) {
                        $scope.ok = function() {
                            $uibModalInstance.close();
                            if (isRemovingRuleGroup) {
                                pillsScope.removeRuleGroup(ruleGroupName);
                            } else {
                                pillsScope.removeRule(ruleGroupName, ruleName);
                            }
                        };

                        $scope.cancel = function() {
                            $uibModalInstance.dismiss('cancel');
                        };
                    }
                });
            };

            $scope.removeRule = function(currentRuleGroupName, currentRuleName) {
                $scope.factoryConstellation.getConstellation()
                    .sendMessageWithSaga({ Scope: "Package", Args: ["Hydra"] },
                        "RemoveRule",
                        [currentRuleGroupName, currentRuleName],
                        function(response) {
                            if (response.Data) {
                                $scope.factoryConstellation.updateRuleGroups();
                                $scope.factoryConstellation.setRuleGroupInSelection($scope.factoryConstellation.getSlogan());
                                $location.path("#/");
                                $scope.message = "The rule has been removed.";
                            } else {
                                $scope.message = "An error occured when removing the rule, the operation has been aborted.";
                            }

                            var parentScope = $scope;
                            var modalInstance = $uibModal.open({
                              templateUrl: "pills/rule-removing-modal.template.html",
                              size: 'sm',
                              controller: function($scope, $uibModalInstance) {
                                $scope.message = parentScope.message;
                              }
                            });
                        });
            };

            $scope.removeRuleGroup = function(currentRuleGroupName) {
                $scope.factoryConstellation.getConstellation()
                .sendMessageWithSaga({ Scope: "Package", Args: ["Hydra"] },
                    "RemoveRuleGroup",
                    currentRuleGroupName,
                    function(response) {
                        if (response.Data) {
                            $scope.factoryConstellation.updateRuleGroups();
                            $scope.factoryConstellation.setRuleGroupInSelection($scope.factoryConstellation.getSlogan());
                            $location.path("#/");
                            $scope.message = "The group has been removed.";
                        } else {
                            $scope.message = "An error occured when removing the group, the operation has been aborted.";
                        }

                        var parentScope = $scope;
                        var modalInstance = $uibModal.open({
                          templateUrl: "pills/rule-removing-modal.template.html",
                          size: 'sm',
                          controller: function($scope, $uibModalInstance) {
                            $scope.message = parentScope.message;
                          }
                        });
                    }
                );
            };
        }
    });
