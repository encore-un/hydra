﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Constellation;
using Constellation.Package;

namespace Hydra
{
    public class StateObjectCollectionWatcher : StateObjectCollectionNotifier
    {
        public event EventHandler<StateObjectUpdatedEventArgs> StateObjectExpired;

        public StateObjectCollectionWatcher()
        {
            Task.Factory.StartNew(() =>
            {
                while (PackageHost.IsRunning)
                {
                    try
                    {
                        foreach (StateObjectNotifier son in this)
                        {
                            if (son.Value.IsExpired && !son.Value.Metadatas.ContainsKey("__Watcher_SeenExpired"))
                            {
                                son.Value.Metadatas.Add("__Watcher_SeenExpired", null);
                                StateObjectExpired?.Invoke(this,
                                    new StateObjectUpdatedEventArgs {StateObject = son.Value});
                            }
                        }
                    }
                    catch
                    {
                        // ignored
                    }

                    Thread.Sleep(1000);
                }
            });
        }
    }
}
