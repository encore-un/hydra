﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Constellation;
using Constellation.Package;
using Newtonsoft.Json.Linq;

namespace Hydra.DataManager
{
    internal static class StateObjectManager
    {
        public static ConcurrentDictionary<Tuple<string, string>, List<StateObject>> StateObjectDictionary { get; } =
            new ConcurrentDictionary<Tuple<string, string>, List<StateObject>>();

        static StateObjectManager()
        {
            PackageHost.StateObjectUpdated += (s, e) =>
            {
                var key = Tuple.Create(e.StateObject.SentinelName, e.StateObject.PackageName);

                // if the key is not already existing, we initialize it to an empty list of SO
                if (!StateObjectDictionary.ContainsKey(key))
                    StateObjectDictionary[key] = new List<StateObject>();

                // if the key is existing, we update the current SO
                StateObjectDictionary[key].RemoveAll(so => so.Name == e.StateObject.Name);
                StateObjectDictionary[key].Add(e.StateObject);
            };
        }

        public static void UpdateStateObjects()
        {
            Parallel.ForEach(PackageManager.PackageNameDictionary, kvp =>
            {
                var sentinelName = kvp.Key;
                var packageNameList = kvp.Value;

                foreach (var packageName in packageNameList)
                {
                    PackageHost.RequestStateObjects(sentinelName, packageName);
                }
            });
        }


        public static List<StateObject> GetStateObjectList(string sentinel, string package)
        {
            if (
                StateObjectDictionary == null)
                throw new DataRetriever.NullAttributeException("State object dictionary null");

            var tuple = Tuple.Create(sentinel, package);

            if (StateObjectDictionary.ContainsKey(tuple))
                return StateObjectDictionary[tuple];

            throw new DataRetriever.KeyNotFoundException("Key not found in State object dictionary");
        }

        public static dynamic GetStateObjectValue(string sentinelName, string packageName, string stateObjectName, string jPath)
        {
            var stateObject = GetStateObject(sentinelName, packageName, stateObjectName);

            dynamic value;

            if (!stateObject.TryGetValue(out value))
                throw new DataRetriever.UnacessibleStateObjectException("Cannot access value of stateObjec");

            if (!(value is JObject)) return value;

            var jObjectSo = (JObject) JObject.FromObject(value);

            return jObjectSo.SelectToken(jPath);
        }

        public static StateObject GetStateObject(string sentinelName, string packageName, string stateObjectName)
        {
            var stateObjectList = GetStateObjectList(sentinelName, packageName);

            foreach (var stateObject in stateObjectList)
            {
                if (stateObject.Name != stateObjectName)
                    continue;

                return stateObject;
            }

            throw new DataRetriever.UnacessibleStateObjectException();
        }
    }
}
