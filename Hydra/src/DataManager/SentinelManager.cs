﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Constellation.Package;

namespace Hydra.DataManager
{
    internal static class SentinelManager
    {
        static SentinelManager()
        {
            // we want to create the event handler only once
            PackageHost.ControlManager.SentinelsListUpdated += (s, e) =>
            {
                SentinelNameList = new ConcurrentBag<string>();
                foreach (var sentinelInfo in e.Sentinels)
                {
                    if(sentinelInfo.Description.SentinelName != "Developer")
                        SentinelNameList.Add(sentinelInfo.Description.SentinelName);
                }
            };
        }

        public static ConcurrentBag<string> SentinelNameList { get; set; } = new ConcurrentBag<string>();

        public static void UpdateSentinelNameList()
        {
            PackageHost.ControlManager.RequestSentinelsList();
        }
    }
}
