﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Constellation.Package;

namespace Hydra.DataManager
{
    internal static class DataRetriever
    {

        public static void DataRetrieverInitializer()
        {
            if(PackageHost.HasControlManager)
                // start a thread to check updates of constellation's datas every 1s
                Task.Factory.StartNew(() =>
                {
                    while (PackageHost.IsRunning)
                    {
                        SentinelManager.UpdateSentinelNameList();
                        PackageManager.UpdatePackageDictionnary();
                        StateObjectManager.UpdateStateObjects();

                        Thread.Sleep(1000);
                    }
                });
            else
                throw new AccesHubDeniedException();
        }

        /// <summary>
        /// Custom exception when an error occured in Data Retriever
        /// </summary>
        public abstract class DataRetrieverException : Exception
        {
            /// <summary>
            /// 0-parameter constructor for the DataRetrieverException
            /// </summary>
            protected DataRetrieverException() : this("DataRetriever Exception thrown")
            {
            }

            /// <summary>
            /// 1-parameter constructor for the DataRetrieverException
            /// </summary>
            /// <param name="msg"></param>
            protected DataRetrieverException(string msg) : base(msg)
            {
            }
        }

        /// <summary>
        /// Custom exception when the hub access is denied, inherited from DataRetrieverException
        /// </summary>
        public class AccesHubDeniedException : DataRetrieverException
        {
            /// <summary>
            /// 0-parameter constructor for the AccessHubDenied exception
            /// </summary>
            public AccesHubDeniedException() : this("Access Hub denied")
            {
            }

            /// <summary>
            /// 1-parameter constructor for the AccessHubDenied exception
            /// </summary>
            /// <param name="msg"></param>
            public AccesHubDeniedException(string msg) : base(msg)
            {
            }
        }

        /// <summary>
        /// Custom exception when an instance is null, inherited from DataRetrieverException
        /// </summary>
        public class NullAttributeException : DataRetrieverException
        {
            /// <summary>
            /// 0-parameter constructor for the NullAttribute exception
            /// </summary>
            public NullAttributeException() : this("Null attribute")
            {
            }

            /// <summary>
            /// 1-paramater constructor for the NullAttribute exception
            /// </summary>
            /// <param name="msg"></param>
            public NullAttributeException(string msg) : base(msg)
            {
            }
        }

        /// <summary>
        /// Custom exception when a key is not found in dictionnaries
        /// </summary>
        public class KeyNotFoundException : DataRetrieverException
        {
            /// <summary>
            /// 0-parameter constructor for the KeyNotFound Exception
            /// </summary>
            public KeyNotFoundException() : this("Key not found")
            {
            }

            /// <summary>
            /// 1-parameter constructor for the KeyNotFound exception
            /// </summary>
            /// <param name="msg"></param>
            public KeyNotFoundException(string msg) : base(msg)
            {
            }
        }

        /// <summary>
        /// Custom exception when the state object doesn't exist
        /// </summary>
        public class UnacessibleStateObjectException : DataRetrieverException
        {
            /// <summary>
            /// 0-parameter constructor for the UnacessibleStateObject exception
            /// </summary>
            public UnacessibleStateObjectException() : this("Value for this state object name doesn't exist")
            {
            }

            /// <summary>
            /// 1-parameter constructor for the UnacessibleStateObject exception
            /// </summary>
            /// <param name="msg"></param>
            public UnacessibleStateObjectException(string msg) : base(msg)
            {
            }
        }
    }
}
