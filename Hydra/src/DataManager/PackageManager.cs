﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Constellation.Package;

namespace Hydra.DataManager
{
    internal static class PackageManager
    {

        public static ConcurrentDictionary<string, List<string>> PackageNameDictionary { get; } = new ConcurrentDictionary<string, List<string>>();

        static PackageManager()
        {
            // we want to create the event handler only once
            PackageHost.ControlManager.PackagesListUpdated += (s, e) =>
            {
                var packages =
                    (from package in e.Packages
                        where package.Package.Name != PackageHost.PackageName
                        select package.Package.Name).ToList();

                PackageNameDictionary[e.SentinelName] = packages;
            };
        }

        public static void UpdatePackageDictionnary()
        {
            foreach (var sentinel in SentinelManager.SentinelNameList)
            {
                PackageHost.ControlManager.RequestPackagesList(sentinel);
                PackageHost.ControlManager.ReceivePackageState = true;
            }
        }

        public static List<string> GetPackagesList(string sentinel)
        {
            if (PackageNameDictionary == null)
                throw new DataRetriever.NullAttributeException("Package dictionary null");

            if (PackageNameDictionary.ContainsKey(sentinel))
                return PackageNameDictionary[sentinel];

            throw new DataRetriever.KeyNotFoundException("Key not found in Package dictionary");
        }
    }
}
