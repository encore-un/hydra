﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Constellation.Package;
using Hydra.Rule;
using Newtonsoft.Json;

namespace Hydra
{
    /// <summary>
    /// 
    /// </summary>
    public static class Store
    {
        private static readonly string RuleGroupFolder =
            Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/rules/";

        /// <summary>
        /// 
        /// </summary>
        public static bool Loaded { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public static Dictionary<string, RuleGroup> RuleGroups { get; } = new Dictionary<string, RuleGroup>();

        static Store()
        {
            Task.Factory.StartNew(() =>
            {
                while (PackageHost.IsRunning)
                {
                    UpdatePresenter();
                    Thread.Sleep(1000);
                }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        public static void LoadAll()
        {
            PackageHost.WriteInfo("Trying to load rules from " + RuleGroupFolder);

            if (Loaded)
            {
                PackageHost.WriteWarn("Trying to load rules twice");
                return;
            }

            // If nothing to load
            if (!Directory.Exists(RuleGroupFolder)) return;

            var ruleGroupDirectories = Directory.GetDirectories(RuleGroupFolder);

            foreach (var ruleGroupDirectory in ruleGroupDirectories)
            {
                var ruleGroup = LoadRuleGroup(ruleGroupDirectory);
                RuleGroups.Add(ruleGroup.Name, ruleGroup);
            }

            Loaded = true;

            UpdatePresenter();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="directoryName"></param>
        /// <returns></returns>
        public static RuleGroup LoadRuleGroup(string directoryName)
        {
            RuleGroup ruleGroup = new RuleGroup();

            if (!File.Exists(directoryName + "/name.txt"))
            {
                PackageHost.WriteError("Cannot find rule group name");
                ruleGroup.Name = directoryName;
            }
            else
            {
                ruleGroup.Name = File.ReadAllText(directoryName + "/name.txt");
            }

            var ruleGroupDirectories = Directory.GetFiles(RuleGroupFolder);

            foreach (var ruleFile in ruleGroupDirectories)
            {
                if(!ruleFile.EndsWith(".json")) continue;
                
                try
                {
                    var rule = LoadRule(ruleFile);
                    ruleGroup.Add(rule);
                }
                catch
                {
                    // ignored
                }
            }
            

            return ruleGroup;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        /// <exception cref="JsonSerializationException"></exception>
        public static Rule.Rule LoadRule(string fileName)
        {
            try
            {
                string file;

                using (var sr = new StreamReader(fileName))
                {
                    file = sr.ReadToEnd();
                }

                var deserializedRule = JsonConvert.DeserializeObject<Rule.Rule>(file,
                    new JsonSerializerSettings
                    {
                        Error = (sender, args) =>
                        {
                            Console.WriteLine("error");
                            args.ErrorContext.Handled = true;
                            throw new JsonSerializationException();
                        }
                    });

                return deserializedRule;
            }
            catch (Exception e)
            {
                PackageHost.WriteError($"Error while deserialize rule at {fileName}");
                PackageHost.WriteError(e.Message);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void SaveAll()
        {
            if(Directory.Exists(RuleGroupFolder))
                Directory.Delete(RuleGroupFolder, true);

            Directory.CreateDirectory(RuleGroupFolder);

            foreach (var keyValuePair in RuleGroups)
            {
                SaveRuleGroup(keyValuePair.Value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void SaveRuleGroup(RuleGroup ruleGroup)
        {

            var invalids = System.IO.Path.GetInvalidFileNameChars();
            var sanitarizedRuleGroupName =
                String.Join("_", ruleGroup.Name.Split(invalids, StringSplitOptions.RemoveEmptyEntries)).TrimEnd('.');

            string directory = $"{RuleGroupFolder}{sanitarizedRuleGroupName}";

            Directory.CreateDirectory(directory);

            File.WriteAllText(directory + "/name.txt", ruleGroup.Name);

            foreach (var rule in ruleGroup.Rules)
            {
                SaveRule(directory, rule);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="rule"></param>
        public static void SaveRule(string directory, Rule.Rule rule)
        {

            var invalids = System.IO.Path.GetInvalidFileNameChars();
            var sanitarizedRuleName =
                String.Join("_", rule.Name.Split(invalids, StringSplitOptions.RemoveEmptyEntries)).TrimEnd('.');

            var fileName = directory + "/" + sanitarizedRuleName + ".json";

            using (var sw = new StreamWriter(fileName, false))
            {
                var serealizedRule = JsonConvert.SerializeObject(
                    rule,
                    Formatting.Indented
                    /*,
                    new JsonSerializerSettings
                    {
                        Converters = new List<JsonConverter>
                        {
                            new ConditionConverter(),
                            new ValueProviderConverter()
                        }
                    }
                    */
                    );

                sw.Write(serealizedRule);
            }
        }

        /// <summary>
        /// Add a rule to Store rulegroup dictionnary
        /// </summary>
        /// <param name="ruleGroup">Rule group name</param>
        /// <param name="rule">Rule to add</param>
        public static bool AddRule(string ruleGroup, Rule.Rule rule)
        {
            // Add rule group if necessary
            AddRuleGroup(ruleGroup);

            if (RuleGroups[ruleGroup].Contains(rule.Name))
                return false;

            rule.Condition.Changed += UpdatePresenter;

            RuleGroups[ruleGroup].Add(rule);

            UpdatePresenter();

            SaveAll();

            return true;
        }

        /// <summary>
        /// Add a rule to Store rulegroup dictionnary
        /// </summary>
        /// <param name="ruleGroup">Rule group name</param>
        /// <param name="rule">Rule to add</param>
        public static bool EditRule(string ruleGroup, Rule.Rule rule)
        {
            RemoveRule(ruleGroup, rule.Name);
            if (!AddRule(ruleGroup, rule))
                return false;

            return true;
        }


        /// <summary>
        /// Add a rule group
        /// </summary>
        public static void AddRuleGroup(string ruleGroup)
        {
            if (RuleGroups.ContainsKey(ruleGroup)) return;

            RuleGroups[ruleGroup] = new RuleGroup
            {
                Name = ruleGroup
            };
        }

        /// <summary>
        /// Remove a rule
        /// </summary>
        /// <param name="ruleGroupName"></param>
        /// <param name="ruleName"></param>
        /// <returns>Return true if a rule have been deleted</returns>
        public static bool RemoveRule(string ruleGroupName, string ruleName)
        {
            if (!RuleGroups.ContainsKey(ruleGroupName)) // Rule group doesn't exist
                return false;

            if (!RuleGroups[ruleGroupName].Contains(ruleName)) // Rule doesn't exist
                return false;

            // Rule to remove
            var rule = RuleGroups[ruleGroupName].GetRule(ruleName);

            rule.Condition.Changed -= UpdatePresenter;

            RuleGroups[ruleGroupName].RemoveRule(rule.Name);

            UpdatePresenter();

            return true;
        }

        /// <summary>
        /// Remove a rule group and all its rules
        /// </summary>
        /// <param name="ruleGroupName"></param>
        /// <returns></returns>
        public static bool RemoveRuleGroup(string ruleGroupName)
        {
            if (!RuleGroups.ContainsKey(ruleGroupName))
                return false;

            foreach (var rule in RuleGroups[ruleGroupName].Rules)
            {
                if (!Store.RemoveRule(ruleGroupName,rule.Name ))
                    return false;
            }

            RuleGroups.Remove(ruleGroupName);

            PackageHost.ControlManager.PurgeStateObjects(PackageHost.SentinelName,PackageHost.PackageName,ruleGroupName);

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void UpdatePresenter()
        {
            foreach (var group in RuleGroups.Values)
            {
                var stateObjectDictionnary = group.Rules.ToDictionary(kvp => kvp.Name, kvp => kvp.Condition.IsValidatedSafe);
               
                PackageHost.PushStateObject(group.Name, stateObjectDictionnary);
            }
            
        }

        /// <summary>
        /// Deserialize a rule with a string parameter
        /// </summary>
        /// <param name="ruleJson"></param>
        /// <returns></returns>
        /// <exception cref="JsonSerializationException"></exception>
        public static Rule.Rule DeserializeRule(string ruleJson)
        {

            return JsonConvert.DeserializeObject<Rule.Rule>(ruleJson, new JsonSerializerSettings
            {
                Error = (sender, args) =>
                {
                    args.ErrorContext.Handled = true;
                    throw new JsonSerializationException();
                }
            });

        }
    }
}