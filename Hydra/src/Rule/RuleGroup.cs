﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Hydra.Rule
{
    /// <summary>
    /// Group of rule
    /// </summary>
    public class RuleGroup
    {
        [JsonProperty("Rules")]
        private Dictionary<string, Rule> _rules = new Dictionary<string, Rule>();

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public List<Rule> Rules
        {
            get { return _rules.Values.ToList(); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rule"></param>
        public void Add(Rule rule)
        {
            _rules.Add(rule.Name, rule);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool Contains(string name)
        {
            return _rules.ContainsKey(name);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ruleName"></param>
        /// <returns></returns>
        public Rule GetRule(string ruleName)
        {
            return _rules[ruleName];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public void RemoveRule(string name)
        {
            _rules.Remove(name);
        }
    }
}