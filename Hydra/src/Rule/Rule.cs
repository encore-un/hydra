﻿using Constellation.Package;
using Newtonsoft.Json;

namespace Hydra.Rule
{
    /// <summary>
    /// Rule
    /// </summary>
    public class Rule
    {
        /// <summary>
        /// Rule condition
        /// </summary>
        [JsonConverter(typeof(ConditionConverter))]
        public Condition.Condition Condition { get; private set; }

        /// <summary>
        /// Rule name
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// 3-arg constructor
        /// </summary>
        /// <param name="condition">
        /// </param>
        /// <param name="name"></param>
        [JsonConstructor]
        public Rule(Condition.Condition condition, string name)
        {
            Condition = condition;
            Name = name;
        }
    }
}