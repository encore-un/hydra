﻿using Hydra.Condition;

namespace Hydra.Value
{
    /// <summary>
    /// ConstantValueProvider class
    /// </summary>
    [ValueProviderName("Constant")]
    public class ConstantValueProvider : IValueProvider
    {
        /// <summary>
        /// On change handler
        /// </summary>
        public event OnChangeHandler Changed;

        /// <summary>
        /// Value
        /// </summary>
        public dynamic Value { get; }

        /// <summary>
        /// 1-arg constructor
        /// </summary>
        /// <param name="value">Value</param>
        public ConstantValueProvider(dynamic value)
        {
            Value = value;
        }
    }
}
