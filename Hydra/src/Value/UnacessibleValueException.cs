﻿using System;

namespace Hydra.Value
{
    /// <summary>
    /// 
    /// </summary>
    public class UnacessibleValueException : Exception
    {
        /// <summary>
        /// 
        /// </summary>
        public UnacessibleValueException()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public UnacessibleValueException(string message) : base(message)
        {
        }
    }
}