﻿using System;
using Hydra.Value;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Hydra
{
    /// <summary>
    /// Value argument converter
    /// </summary>
    public class ValueProviderConverter : JsonConverter
    {
        /// <summary>
        /// ConditionNameAttribute
        /// </summary>
        public const string ConditionNameAttribute = "Provider";

        /// <summary>Writes the JSON representation of the object.</summary>
        /// <param name="writer">The <see cref="T:Newtonsoft.Json.JsonWriter" /> to write to.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializer">The calling serializer.</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var t = JToken.FromObject(value);

            if (t.Type != JTokenType.Object)
            {
                t.WriteTo(writer);
            }
            else
            {
                var o = (JObject)t;

                var providerNameAttribute = ((ValueProviderNameAttribute) value.GetType().GetCustomAttributes(typeof(ValueProviderNameAttribute), true)[0]);

                if(providerNameAttribute == null)
                    throw new Exception("A provider must have a ValueProviderNameAttribute");

                o.Add(ConditionNameAttribute, providerNameAttribute.Name);
                o.WriteTo(writer);
            }

        }

        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>
        /// 	<c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanConvert(Type objectType) => typeof(IValueProvider).IsAssignableFrom(objectType);

        /// <summary>Reads the JSON representation of the object.</summary>
        /// <param name="reader">The <see cref="T:Newtonsoft.Json.JsonReader" /> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existingValue">The existing value of object being read.</param>
        /// <param name="serializer">The calling serializer.</param>
        /// <returns>The object value.</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            if (reader.TokenType != JsonToken.StartObject)
                return null;

            // Load JObject from stream
            JObject jObject = JObject.Load(reader);

            string name = (string)jObject[ConditionNameAttribute];

            if (jObject[ConditionNameAttribute] == null)
            {
                throw new JsonSerializationException("A value provider must have a name");
            }

            Type valueProvider = ValueProviderRetriever.GetValueProvider(name);

            if (valueProvider == null)
                throw new JsonSerializationException("Unknown value provider");

            return jObject.ToObject(valueProvider);
        }

    }
}