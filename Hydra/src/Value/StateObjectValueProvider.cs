﻿using Hydra.Condition;
using Hydra.DataManager;
using Newtonsoft.Json;

namespace Hydra.Value
{
    /// <summary>
    /// State object variable class
    /// </summary>
    [ValueProviderName("StateObjectVariable")]
    public class StateObjectValueProvider : IValueProvider
    {
        /// <summary>
        /// On change handler
        /// </summary>
        public event OnChangeHandler Changed;

        /// <summary>
        /// Sentinel name
        /// </summary>
        public string SentinelName { get; }

        /// <summary>
        /// Package name
        /// </summary>
        public string PackageName { get; }

        /// <summary>
        /// State object name
        /// </summary>
        public string StateObjectName { get; }

        /// <summary>
        /// XPath
        /// </summary>
        public string JPath { get; }


        /// <summary>
        /// Value
        /// </summary>
        [JsonIgnore]
        public dynamic Value => StateObjectManager.GetStateObjectValue(SentinelName, PackageName, StateObjectName, JPath);

        /// <summary>
        /// 4-arg constructor
        /// </summary>
        /// <param name="sentinelName">Sentinel conditionName</param>
        /// <param name="packageName">Package conditionName</param>
        /// <param name="stateObjectName">State object conditionName</param>
        /// <param name="jPath">XPath</param>
        public StateObjectValueProvider(string sentinelName, string packageName, string stateObjectName, string jPath)
        {
            SentinelName = sentinelName;
            PackageName = packageName;
            StateObjectName = stateObjectName;
            JPath = jPath;

            SubscribeManager.AddStateObjectSubscribing(this);
        }

        /// <summary>
        /// On changed
        /// </summary>
        public virtual void OnChanged() => Changed?.Invoke();
    }
}
