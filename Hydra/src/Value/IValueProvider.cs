using Hydra.Condition;

namespace Hydra.Value
{
    /// <summary>
    /// Value argument
    /// </summary>
    public interface IValueProvider : IConditionArgument
    {
        /// <summary>
        /// Value
        /// </summary>
        dynamic Value { get; }
    }
}