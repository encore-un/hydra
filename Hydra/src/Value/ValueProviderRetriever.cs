﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Hydra.Value
{
    /// <summary>
    /// Retrieve condition based on type accepted
    /// </summary>
    public static class ValueProviderRetriever
    {
        /// <summary>
        /// Condition dictionnary (string to type)
        /// </summary>
        public static IDictionary<string, Type> ValueProviders { get; }

        static ValueProviderRetriever()
        {
            ValueProviders =
                Assembly.GetAssembly(typeof(IValueProvider))
                    .GetTypes()
                    .Where(
                        t =>
                            typeof(IValueProvider).IsAssignableFrom(t) &&
                            t.GetCustomAttribute(typeof(ValueProviderNameAttribute), true) != null)
                    .ToDictionary(
                        t => ((ValueProviderNameAttribute)t.GetCustomAttribute(typeof(ValueProviderNameAttribute), true)).Name,
                        t => t);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Type GetValueProvider(string name)
        {
            return ValueProviders[name];
        }
    }
}