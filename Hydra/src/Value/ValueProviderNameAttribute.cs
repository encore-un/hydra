﻿using System;

namespace Hydra.Value
{
    /// <summary>
    /// 
    /// </summary>
    public class ValueProviderNameAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public ValueProviderNameAttribute(string name)
        {
            Name = name;
        }
    }
}