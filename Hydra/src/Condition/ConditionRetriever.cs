﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Hydra.Condition
{
    /// <summary>
    /// Retrieve condition based on type accepted
    /// </summary>
    public static class ConditionRetriever
    {
        /// <summary>
        /// Condition dictionnary (string to type)
        /// </summary>
        public static IDictionary<string, Type> Conditions { get; }

        static ConditionRetriever()
        {
            Conditions =
                Assembly.GetAssembly(typeof(Condition))
                    .GetTypes()
                    .Where(
                        t =>
                            typeof(Condition).IsAssignableFrom(t) &&
                            t.GetCustomAttribute(typeof(ConditionNameAttribute), true) != null)
                    .ToDictionary(
                        t => ((ConditionNameAttribute) t.GetCustomAttribute(typeof(ConditionNameAttribute), true)).Name,
                        t => t);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Type GetCondition(string name)
        {
            return Conditions[name];
        }
    }
}