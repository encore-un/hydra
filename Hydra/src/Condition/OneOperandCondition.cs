﻿using Hydra.Value;
using Newtonsoft.Json;

namespace Hydra.Condition
{
    /// <summary>
    /// One operand rule, example :
    /// isTrue, isFalse, isPositive, ...
    /// </summary>
    /// <typeparam name="T">Operand type</typeparam>
    public abstract class OneOperandCondition <T> : Condition
    {
        /// <summary>
        /// First operand
        /// </summary>
        [JsonConverter(typeof(ValueProviderConverter))]
        [ConditionArgument("Operand")]
        public IValueProvider Operand { get; private set; }

        /// <summary>
        /// 1-arg constructor
        /// </summary>
        /// <param name="operand">First operand</param>
        protected OneOperandCondition(IValueProvider operand)
        {
            Operand = operand;
        }
    }
}