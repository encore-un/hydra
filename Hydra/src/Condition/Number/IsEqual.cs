﻿using Hydra.Value;

namespace Hydra.Condition.Number
{
    /// <summary>
    /// 
    /// </summary>
    [ConditionName("IsEqual")]
    public sealed class IsEqual : TwoOperandCondition
    {
        /// <summary>
        /// 2-arg constructor
        /// </summary>
        /// <param name="firstOperand">First operand</param>
        /// <param name="secondOperand">Second operand</param>
        public IsEqual(IValueProvider firstOperand, IValueProvider secondOperand)
            : base(firstOperand, secondOperand)
        {
            
        }

        /// <summary>
        /// Get the result of the condition
        /// </summary>
        /// <returns>True if condition is met, false otherwise</returns>

        public override bool IsValidated => FirstOperand.Value.Equals(SecondOperand.Value);
    }
}
