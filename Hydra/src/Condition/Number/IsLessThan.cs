﻿using System;
using Hydra.Value;

namespace Hydra.Condition.Number
{
    /// <summary>
    /// Is less than condition
    /// </summary>
    [ConditionName("IsLessThan")]
    public sealed class IsLessThan : TwoOperandCondition
    {
        /// <summary>
        /// 2-arg constructor
        /// </summary>
        /// <param name="firstOperand">First operand</param>
        /// <param name="secondOperand">Second operand</param>
        public IsLessThan(IValueProvider firstOperand,
            IValueProvider secondOperand) : base(firstOperand, secondOperand)
        {
                
        }

        /// <summary>
        /// Get the result of the condition
        /// </summary>
        /// <returns>True if condition is met, false otherwise</returns>
        public override bool IsValidated
        {
            get
            {
                decimal firstValue, secondValue;

                if (FirstOperand.Value is string)
                    firstValue = decimal.Parse(FirstOperand.Value);
                else
                    firstValue = FirstOperand.Value;

                if (SecondOperand.Value is string)
                    secondValue = decimal.Parse(SecondOperand.Value);
                else
                    secondValue = SecondOperand.Value;

                return firstValue > secondValue;
            }
        }
    }
}
