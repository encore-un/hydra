﻿namespace Hydra.Condition
{
    /// <summary>
    /// Common ancestor of Constant and firstOperand
    /// </summary>
    public interface IConditionArgument
    {
        /// <summary>
        /// On change handler
        /// </summary>
        event OnChangeHandler Changed;
    }

    /// <summary>
    /// On change event handler
    /// </summary>
    public delegate void OnChangeHandler();
}
