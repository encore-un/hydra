﻿using Hydra.Value;
using Newtonsoft.Json;

namespace Hydra.Condition
{
    /// <summary>
    /// Two operand rule
    /// </summary>
    public abstract class TwoOperandCondition : Condition
    {

        /// <summary>
        /// First operand
        /// </summary>
        [JsonConverter(typeof(ValueProviderConverter))]
        [ConditionArgument("FirstOperand")]
        public IValueProvider FirstOperand { get; private set; }

        /// <summary>
        /// Second operand
        /// </summary>
        [JsonConverter(typeof(ValueProviderConverter))]
        [ConditionArgument("SecondOperand")]
        public IValueProvider SecondOperand { get; private set; }

        /// <summary>
        /// Get condition result for these operands
        /// </summary>
        /// <param name="firstOperand">First Operand</param>
        /// <param name="secondOperand">Second Operand</param>
        protected TwoOperandCondition(IValueProvider firstOperand, IValueProvider secondOperand)
        {
            FirstOperand = firstOperand;
            SecondOperand = secondOperand;

            firstOperand.Changed += FireChanged;
            secondOperand.Changed += FireChanged;
        }
    }
}