﻿using System;
using Hydra.Condition;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Hydra
{
    /// <summary>
    /// Condition converter
    /// </summary>
    public class ConditionConverter : JsonConverter
    {
        /// <summary>
        /// ConditionNameAttribute
        /// </summary>
        public const string ConditionNameAttribute = "ConditionName";

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var jObject = GetObject((Condition.Condition) value);
            jObject.WriteTo(writer);
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(Condition.Condition).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            // Load JObject from stream
            JObject jObject = JObject.Load(reader);

            return GetCondition(jObject);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static JObject GetObject(Condition.Condition condition)
        {
            JObject jObject = (JObject)JToken.FromObject(condition);

            var conditionNameAttribute = (ConditionNameAttribute)condition.GetType().GetCustomAttributes(typeof(ConditionNameAttribute), true)[0];

            if (conditionNameAttribute == null)
                throw new Exception("A provider must have a ConditionNameAttribute");

            jObject.Add(ConditionNameAttribute, conditionNameAttribute.Name);

            return jObject;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jObject"></param>
        /// <returns></returns>
        /// <exception cref="JsonSerializationException"></exception>
        public static Condition.Condition GetCondition(JObject jObject)
        {
            string conditionName = (string)jObject[ConditionNameAttribute];

            if (jObject[ConditionNameAttribute] == null)
            {
                //return null;
                throw new JsonSerializationException("A condition must have a name");
            }

            Type conditionType = ConditionRetriever.GetCondition(conditionName);

            if (conditionType == null)
                throw new JsonSerializationException("Unknown condition name");

            return (Condition.Condition) jObject.ToObject(conditionType);
        }
    }
}