﻿using Hydra.Value;

namespace Hydra.Condition.Boolean
{
    /// <summary>
    /// Is true condition
    /// </summary>
    [ConditionName("IsTrue")]
    public sealed class IsTrue : OneOperandCondition<bool>
    {
        /// <summary>
        /// 1-arg constructor
        /// </summary>
        /// <param name="operand">Operand</param>
        public IsTrue(IValueProvider operand) : base(operand)
        {
            
        }

        /// <summary>
        /// Is condition validated
        /// </summary>
        public override bool IsValidated
        {
            get
            {
                if(Operand.Value is string)
                    return System.Boolean.Parse(Operand.Value);

                return Operand.Value;
            }
        }
    }
}