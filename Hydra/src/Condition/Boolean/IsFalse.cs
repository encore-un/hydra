﻿using Hydra.Value;

namespace Hydra.Condition.Boolean
{
    /// <summary>
    /// Is false condition
    /// </summary>
    [ConditionName("IsFalse")]
    public sealed class IsFalse : OneOperandCondition<bool>
    {
        /// <summary>
        /// 1-arg constructor
        /// </summary>
        /// <param name="operand">Operand</param>
        public IsFalse(IValueProvider operand) : base(operand)
        {
            
        }

        /// <summary>
        /// Is condition validated
        /// </summary>
        public override bool IsValidated
        {
            get
            {
                if (Operand.Value is string)
                    return !System.Boolean.Parse(Operand.Value);

                return !Operand.Value;
            }
        }
    }
}