﻿using System;

namespace Hydra.Condition
{
    /// <summary>
    /// 
    /// </summary>
    internal class ConditionNameAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public ConditionNameAttribute(string name)
        {
            Name = name;
        }
    }
}