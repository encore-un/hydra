﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Hydra
{
    /// <summary>
    /// Condition converter
    /// </summary>
    public class ConditionListConverter : JsonConverter
    {
        /// <summary>
        /// ConditionNameAttribute
        /// </summary>
        public const string ConditionNameAttribute = "ConditionName";

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            List<Condition.Condition> conditions = (List<Condition.Condition>) value;

            var jArray = new JArray();

            foreach (var rectangle in conditions)
            {
                jArray.Add(ConditionConverter.GetObject(rectangle));
            }

            jArray.WriteTo(writer);
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(List<Condition.Condition>).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            // Load JObject from stream
            JArray jArray = JArray.Load(reader);

            List<Condition.Condition> conditions = new List<Condition.Condition>();

            foreach (JToken jToken in jArray)
            {
                conditions.Add(ConditionConverter.GetCondition((JObject) jToken));
            }

            return conditions;
        }

    }
}