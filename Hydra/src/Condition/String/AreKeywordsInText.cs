﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Hydra.Condition.String
{
    /// <summary>
    /// Detect if multiples words are in text
    /// </summary>
    [ConditionName("AreKeywordsInText")]
    public class AreKeywordsInText : Condition
    {
        /// <summary>
        /// Text
        /// </summary>
        public string Text { get; }

        /// <summary>
        /// Keywords
        /// </summary>
        public List<string> Keywords { get; }

        /// <summary>
        /// 2-arg constructor
        /// </summary>
        /// <param name="text">Text</param>
        /// <param name="keywords">Keywords</param>
        public AreKeywordsInText(string text, List<string> keywords)
        {
            Text = text;
            Keywords = keywords;
        }

        /// <summary>
        /// Get the result of the condition
        /// </summary>
        /// <returns>True if condition is met, false otherwise</returns>
        [JsonIgnore]
        public bool Result
        {
            get { return Keywords.Any(keyword => Text.Contains(keyword)); }
        }

        public sealed override bool IsValidated { get; }
    }
}
