﻿namespace Hydra.Condition.String
{
    /// <summary>
    /// Determine if keyword is in a text
    /// </summary>
    [ConditionName("IsKeywordInText")]
    public sealed class IsKeywordInText : Condition
    {
        /// <summary>
        /// Keyword
        /// </summary>
        public string Keyword { get; }
        
        /// <summary>
        /// Text
        /// </summary>
        public string Text { get; }

        /// <summary>
        /// 2-arg constructor
        /// </summary>
        public IsKeywordInText(string keyword, string text)
        {
            Keyword = keyword;
            Text = text;
        }

        /// <summary>
        /// Name of the rule, to be displayed when choosing rule
        /// </summary>
        public string Name => "Is keyword in text";

        /// <summary>
        /// Get the result for given operands
        /// </summary>
        /// <returns></returns>
        public bool Result => Text.Contains(Keyword);

        public override bool IsValidated { get; }
    }
}
