﻿using System;

namespace Hydra.Condition
{
    /// <summary>
    /// 
    /// </summary>
    public class ConditionArgumentAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public string ConditionName { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conditionName"></param>
        /// <param name="type"></param>
        public ConditionArgumentAttribute(string conditionName)
        {
            ConditionName = conditionName;
        }
    }
}