﻿using System;
using Constellation.Package;
using Newtonsoft.Json;
using static Hydra.DataManager.DataRetriever;

namespace Hydra.Condition
{
    /// <summary>
    /// Condition class
    /// </summary>
    public abstract class Condition : IConditionArgument
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual bool DefaultValue { get; } = false;

        /// <summary>
        /// On change handler
        /// </summary>
        public event OnChangeHandler Changed;

        /// <summary>
        /// Is condition validated
        /// </summary>
        [JsonIgnore]
        public abstract bool IsValidated { get; }

        /// <summary>
        /// Get value safely, if not able to get value, return default value
        /// </summary>
        [JsonIgnore]
        public bool IsValidatedSafe
        {
            get
            {
                bool safeValue;

                try
                {
                    safeValue = IsValidated;
                }
                catch (DataRetrieverException)
                {
                    safeValue = DefaultValue;
                    PackageHost.WriteInfo("Cannot find value in constellation");
                }
                catch (Exception e)
                {
                    safeValue = DefaultValue;
                    PackageHost.WriteWarn("Error while getting value from ValueProvider : " + e.Message);
                }

                return safeValue;
            }
        }

        /// <summary>
        /// Previous result, used to determine if the result have changed
        /// </summary>
        [JsonIgnore]
        protected bool PreviousResult { get; set; }

        /// <summary>
        /// Fire on change
        /// </summary>
        protected void FireChanged()
        {
            Changed?.Invoke();
        }
    }

    /// <summary>
    /// On invalidation handler
    /// </summary>
    public delegate void InvalidationHandler();

    /// <summary>
    /// On validation handler
    /// </summary>
    public delegate void ValidationHandler();
}
