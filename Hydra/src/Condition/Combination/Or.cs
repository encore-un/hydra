﻿using Newtonsoft.Json;

namespace Hydra.Condition.Combination
{
    /// <summary>
    /// Combine 2 condition by and
    /// </summary>
    [ConditionName("Or")]
    public sealed class Or : Condition
    {
        /// <summary>
        /// First condition
        /// </summary>
        [ConditionArgument("FirstCondition")]
        [JsonConverter(typeof(ConditionConverter))]
        public Condition FirstCondition { get; }

        /// <summary>
        /// Second condition
        /// </summary>
        [ConditionArgument("SecondCondition")]
        [JsonConverter(typeof(ConditionConverter))]
        public Condition SecondCondition { get; }

        /// <summary>
        /// 2-arg constructor
        /// </summary>
        /// <param name="firstCondition">First condition</param>
        /// <param name="secondCondition">Second condition</param>
        public Or(Condition firstCondition, Condition secondCondition)
        {
            FirstCondition = firstCondition;
            SecondCondition = secondCondition;

            firstCondition.Changed += FireChanged;
            secondCondition.Changed += FireChanged;
        }

        /// <summary>
        /// Is condition validated
        /// </summary>
        public override bool IsValidated => FirstCondition.IsValidatedSafe || SecondCondition.IsValidatedSafe;
    }
}