﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Hydra.Condition.Combination
{
    /// <summary>
    /// Combine 2 condition by and
    /// </summary>
    [ConditionName("MultipleOr")]
    public sealed class MultipleOr : Condition
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonConverter(typeof(ConditionListConverter))]
        [ConditionArgument("Conditions")]
        public List<Condition> Conditions { get; }

        /// <summary>
        /// 1-arg constructor
        /// </summary>
        public MultipleOr(List<Condition> conditions)
        {
            Conditions = conditions;

            foreach (var condition in conditions)
            {
                condition.Changed += FireChanged;
            }
        }

        /// <summary>
        /// Is condition validated
        /// </summary>
        public override bool IsValidated
        {
            get
            { return Conditions.Any(condition => condition.IsValidatedSafe); }
        }
    }
}