﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Hydra.Condition.Combination
{
    /// <summary>
    /// Combine 2 condition by and
    /// </summary>
    [ConditionName("MultipleAnd")]
    public sealed class MultipleAnd : Condition
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonConverter(typeof(ConditionListConverter))]
        [ConditionArgument("Conditions")]
        public List<Condition> Conditions { get; }

        /// <summary>
        /// 1-arg constructor
        /// </summary>
        public MultipleAnd(List<Condition> conditions)
        {
            Conditions = conditions;

            foreach (var condition in conditions)
            {
                condition.Changed += FireChanged;
            }
        }

        /// <summary>
        /// Is condition validated
        /// </summary>
        public override bool IsValidated
        {
            get
            { return Conditions.All(condition => condition.IsValidatedSafe); }
        }
    }
}