﻿using System;
using System.Collections.Generic;
using System.Linq;
using Constellation.Package;
using Hydra.Condition;
using Hydra.Rule;
using Constellation;
using Hydra.Condition.Combination;
using Hydra.Condition.Number;
using Hydra.DataManager;
using Hydra.Schema;
using Hydra.Value;
using Newtonsoft.Json;
using Newtonsoft.Json.Schema;

namespace Hydra
{
    /// <summary>
    /// Entry point
    /// </summary>
    public class Program : PackageBase
    {
        /// <summary>
        /// Entry point
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            PackageHost.Start<Program>(args);
        }

        /// <summary>Entry Point : Called when the package is started.</summary>
        public override void OnStart()
        {
            DataRetriever.DataRetrieverInitializer();

            Store.LoadAll();

            AddTestRules(); // TODO: Remove
        }

        /// <summary>
        /// 
        /// </summary>
        [MessageCallback]
        public void AddTestRules()
        {
            Store.AddRule("TemperatureHumidityChecker",
                new Rule.Rule(new IsLessThan(new ConstantValueProvider(20), new StateObjectValueProvider("ISENMIC-CM51M0H", "GenerateObject", "TemperatureHumidity", "$.Humidity")), "Humidity"));

            Store.AddRule("TemperatureHumidityChecker",
                new Rule.Rule(new IsLessThan(new ConstantValueProvider(20), new StateObjectValueProvider("ISENMIC-CM51M0H", "GenerateObject", "TemperatureHumidity", "$.Temperature")), "Temperature"));
        }

        /// <summary>
        /// Called when the package is shutdown (disconnected from Constellation)
        /// </summary>
        public override void OnShutdown()
        {
            Store.SaveAll();
        }

        /// <summary>
        /// Get All rules
        /// </summary>
        /// <returns></returns>
        [MessageCallback]
        public Dictionary<string, RuleGroup> GetRules()
        {
            return Store.RuleGroups;
        }

        /// <summary>
        /// Add a rule
        /// </summary>
        public bool AddRule(string ruleGroupName, Rule.Rule rule)
        {
            return Store.AddRule(ruleGroupName, rule);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ruleGroupName"></param>
        /// <param name="ruleJson"></param>
        [MessageCallback]
        public bool AddRule(string ruleGroupName, string ruleJson)
        {
            try
            {
                return AddRule(ruleGroupName, Store.DeserializeRule(ruleJson));
            }
            catch (JsonSerializationException e)
            {
                PackageHost.WriteInfo("JSON Serialization Exception");
                PackageHost.WriteInfo(e.Message);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ruleGroupName"></param>
        /// <param name="ruleJson"></param>
        [MessageCallback]
        public bool EditRule(string ruleGroupName, string ruleJson)
        {
            try
            {
                return Store.EditRule(ruleGroupName, Store.DeserializeRule(ruleJson));
            }
            catch (JsonSerializationException)
            {
                PackageHost.WriteInfo("JSON Serialization exception");
                return false;
            }
            catch (Exception e)
            {
                PackageHost.WriteInfo(e.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// Message callback to remove a rule from a rule group
        /// </summary>
        /// <param name="ruleGroupName"></param>
        /// <param name="ruleName"></param>
        /// <returns>Return if a rule have been deleted</returns>
        [MessageCallback]
        public bool RemoveRule(string ruleGroupName, string ruleName)
        {
            return Store.RemoveRule(ruleGroupName, ruleName);
        }

        /// <summary>
        /// Message callback to remove a rule group and all its rules
        /// </summary>
        /// <param name="ruleGroupName"></param>
        /// <returns></returns>
        [MessageCallback]
        public bool RemoveRuleGroup(string ruleGroupName)
        {
            return Store.RemoveRuleGroup(ruleGroupName);
        }

        /// <summary>
        /// Get Conditions
        /// </summary>
        /// <returns></returns>
        [MessageCallback]
        public Dictionary<string, JSchema> GetConditionsSchemas()
        {
            var generator = new SchemaGenerator();
            return ConditionRetriever.Conditions.ToDictionary(kvp => kvp.Key, kvp => generator.Generate(kvp.Value));
        }

        /// <summary>
        /// Get Conditions
        /// </summary>
        /// <returns></returns>
        [MessageCallback]
        public JSchema GetConditionSchema(string conditionName)
        {
            var generator = new SchemaGenerator();
            return generator.Generate(ConditionRetriever.Conditions[conditionName]);
        }

        /// <summary>
        /// Message callback to get a list of packages deployed on a sentinel
        /// </summary>
        /// <param name="sentinelName"></param>
        /// <returns></returns>
        [MessageCallback]
        public List<string> GetPackagesList(string sentinelName)
        {
            return PackageManager.GetPackagesList(sentinelName);
        }

        /// <summary>
        /// Message callback to get a list of state objects generated by a combinary key "sentinel-package"
        /// </summary>
        /// <param name="sentinelName"></param>
        /// <param name="packageName"></param>
        /// <returns></returns>
        [MessageCallback]
        public List<StateObject> GetStateObjectList(string sentinelName, string packageName)
        {
            return StateObjectManager.GetStateObjectList(sentinelName, packageName);
        }
    }
}
