﻿using System;
using System.Collections;
using System.Reflection;
using Hydra.Condition;
using Hydra.Value;
using Newtonsoft.Json.Schema;

namespace Hydra.Schema
{
    /// <summary>
    /// 
    /// </summary>
    public class SchemaGenerator
    {
        /// <summary>
        /// Generate a schema for a type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public JSchema Generate(Type type)
        {
            // Little trick to avoid json.net limitation

            if (typeof(Condition.Condition).IsAssignableFrom(type))
                return GenerateConditionSchema(type);

            if (typeof(IValueProvider).IsAssignableFrom(type))
                return GenerateValueProviderSchema(type);

            if (typeof(IList).IsAssignableFrom(type))
            {
                return new JSchema
                {
                    Type = JSchemaType.Array,
                    Items = { Generate(type.GetGenericArguments()[0]) }
                };
            }

            throw new Exception("Cannot create shema for this type :  " + type.Name);
        }

        private JSchema GenerateConditionSchema(Type type)
        {
            JSchema schema = new JSchema
            {
                Type = JSchemaType.Object,
                Properties =
                {
                    { "ConditionName", new JSchema {Type = JSchemaType.String} }
                },
                Required = { "ConditionName" }
            };

            foreach (var propertyInfo in type.GetProperties())
            {
                var attribute = (ConditionArgumentAttribute) propertyInfo.GetCustomAttribute(typeof(ConditionArgumentAttribute), true);


                if (attribute == null) continue;

                schema.Properties.Add(attribute.ConditionName, Generate(propertyInfo.PropertyType));
                schema.Required.Add(attribute.ConditionName);
            }

            return schema;
        }

        private JSchema GenerateValueProviderSchema(Type type)
        {
            JSchema schema = new JSchema
            {
                Type = JSchemaType.Object,
                Properties =
                {
                    { "Provider", new JSchema {Type = JSchemaType.String} }
                },
            };

            //schema.Properties.Add();

            return schema;
        }
    }
}