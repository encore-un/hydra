﻿using Constellation;
using Constellation.Package;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HydraDemo
{
    public class Program : PackageBase
    {
        private object isComputerOn;

        static void Main(string[] args)
        {
            PackageHost.Start<Program>(args);
        }

        public override void OnStart()
        {
            var manager = new BedroomManager();
            Task.Factory.StartNew(() =>
            {
                while (PackageHost.IsRunning)
                {

                    PackageHost.PushStateObject("BedroomManager", manager);
                    manager.areShuttersClosed = !manager.areShuttersClosed;
                    Thread.Sleep(3000);
                    PackageHost.PushStateObject("BedroomManager", manager);
                    manager.areShuttersClosed = !manager.areShuttersClosed;
                    PackageHost.PushStateObject("BedroomManager", manager);
                    manager.isComputerOn = !manager.isComputerOn;
                    Thread.Sleep(3000);
                }
            });
        }

        public class BedroomManager
        {
            public bool areShuttersClosed = true;
            public bool isComputerOn = true;
        }
    }
}
